var syntax        = 'sass';

var gulp          = require('gulp'),
		sass          = require('gulp-sass'),
		browserSync   = require('browser-sync'),
		concat        = require('gulp-concat'),
		uglify        = require('gulp-uglify'),
		cleancss      = require('gulp-clean-css'),
		// rename        = require('gulp-rename'),
		// imagemin       = require('gulp-imagemin'),
		// cache          = require('gulp-cache'),
		del            = require('del'),
		autoprefixer  = require('gulp-autoprefixer'),
		pug 		   = require('gulp-pug'),
		notify        = require('gulp-notify'),
		sourcemaps = require('gulp-sourcemaps');

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
	// browserSync({
	// 	proxy: "domen/index.php",
	// 	notify: false
	// });
});

gulp.task('styles', function() {
	return gulp.src('app/assets/'+syntax+'/**/*.'+syntax+'')
	.pipe(sourcemaps.init())
	.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
	.pipe(autoprefixer(['last 10 versions']))
	// .pipe(rename({ suffix: '.min', prefix : '' }))
	// .pipe(cleancss( {level: { 1: { specialComments: 0 } } }))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('app/assets/css'))
	.pipe(browserSync.stream());
});

gulp.task('scripts', function() {
	return gulp.src([
		'app/assets/include/libs/jquery/jquery-3.4.1.min.js',
		'app/assets/include/libs/swiper-slider/swiper-bundle.min.js',
		'app/assets/include/libs/fancybox-master/jquery.fancybox.min.js',
		'app/assets/include/common.js',
		])
	.pipe(concat('scripts.js'))
	// .pipe(uglify())
	.pipe(gulp.dest('app/assets/js'))
	.pipe(browserSync.reload({ stream: true }));
});


// Сборка проекта
gulp.task('clearcache', function () { return cache.clearAll(); });
gulp.task('imagemin', function() {
	return gulp.src('app/assets/img/**/*')
	// .pipe(cache(imagemin()))
	.pipe(gulp.dest('dist/img')); 
});

gulp.task('removedist', function() { return del(['dist'], { force: true }) });

gulp.task('buildHtml', function() { return gulp.src(['app/*.html']).pipe(gulp.dest('dist')) });
gulp.task('buildCss', function() { return gulp.src(['app/assets/css/*.css']).pipe(gulp.dest('dist/assets/css')) });
gulp.task('buildJs', function() { return gulp.src(['app/assets/js/*.js']).pipe(gulp.dest('dist/assets/js')) });
gulp.task('buildFonts', function() { return gulp.src(['app/assets/fonts/**/*']).pipe(gulp.dest('dist/assets/fonts')) });
gulp.task('buildLibs', function() { return gulp.src(['app/assets/libs/**/*']).pipe(gulp.dest('dist/assets/libs')) });

// gulp.task('removePackageLock', function() { return del(['package-lock.json'], { force: true }) });

gulp.task('dist', gulp.series('removedist', 'imagemin', 'styles', 'scripts', 'buildHtml', 'buildCss', 'buildJs', 'buildFonts', 'buildLibs'));


// HTML
// gulp.task('code', function() {
// 	return gulp.src('app/*.html')
// 	.pipe(browserSync.reload({ stream: true }));
// });


// PUG
gulp.task('pugCompile', function() {
	return gulp.src('app/assets/pug/*.pug')
	.pipe(pug({pretty: true}))
	.pipe(gulp.dest('app'))
	.pipe(browserSync.reload({ stream: true }));
});
// gulp.task('pugClean', function () {
// 	return del('app/pug-modules', {force:true});
// });
// gulp.task('code', gulp.series('pugCompile', 'pugClean'));


gulp.task('watch', function() {
	gulp.watch('app/assets/'+syntax+'/**/*.'+syntax+'', gulp.parallel('styles'));
	gulp.watch(['app/assets/include/common.js'], gulp.parallel('scripts'));
	// gulp.watch('app/*.html', gulp.parallel('code'));
	gulp.watch(['app/assets/pug/**/*.pug'], gulp.parallel('pugCompile'));
});

gulp.task('default', gulp.parallel('watch', 'styles', 'scripts', 'browser-sync'));